import { auth } from './auth'

export const signUp = (email, password) => auth.createUserWithEmailAndPassword(email, password)
