import { auth } from './auth'
import { dispatchToStore } from 'utils/compositeActions'

export const listenUserChange = () =>
  auth.onAuthStateChanged(usuarioAtual => {
    dispatchToStore('select', 'usuarioAtual', usuarioAtual)
    dispatchToStore('select', 'loadingUser', false)
  })
