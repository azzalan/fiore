import { matchCollection } from './matchCollection'
import { turnCollection } from './turnCollection'

export const match = matchId => {
  const matchRef = matchCollection.doc(matchId)
  matchRef.turns = turnCollection(matchRef)
  return matchRef
}
