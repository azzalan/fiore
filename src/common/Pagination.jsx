import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withStrings } from 'local-modules/strings'

import Table from '@material-ui/core/Table'
import TableFooter from '@material-ui/core/TableFooter'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'

class PaginationComponent extends Component {
  static propTypes = {
    quantidadeTotal: PropTypes.number.isRequired,
    onChangePagina: PropTypes.func,
    onChangeQuantidadePorPagina: PropTypes.func,
    quantidadePorPaginaInicial: PropTypes.number,
    paginaInicial: PropTypes.number,
    quantidadePorPaginaOpcoes: PropTypes.array,
    // strings
    strings: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      paginaAtual: props.paginaInicial || 0,
      quantidadePorPagina: props.quantidadePorPaginaInicial || 10,
    }
  }

  onChangePagina = (e, numeroDaPagina) => {
    const { onChangePagina } = this.props
    this.setState({ paginaAtual: numeroDaPagina })
    if (onChangePagina) onChangePagina(numeroDaPagina)
  }

  onChangeQuantidadePorPagina = e => {
    const { onChangeQuantidadePorPagina } = this.props
    const quantidadePorPagina = e.target.value
    this.setState({ quantidadePorPagina })
    if (onChangeQuantidadePorPagina) this.props.onChangeQuantidadePorPagina(quantidadePorPagina)
  }

  labelDisplayedRows = ({ from, to, count }) => `${from}-${to} ${this.props.strings.de} ${count}`

  render() {
    const { strings, quantidadeTotal, quantidadePorPaginaOpcoes } = this.props
    const { paginaAtual, quantidadePorPagina } = this.state
    return (
      <Table>
        <TableFooter>
          <TableRow>
            <TablePagination
              labelRowsPerPage={strings.porPagina}
              count={quantidadeTotal}
              rowsPerPage={quantidadePorPagina}
              page={paginaAtual}
              onChangePage={this.onChangePagina}
              onChangeRowsPerPage={this.onChangeQuantidadePorPagina}
              rowsPerPageOptions={quantidadePorPaginaOpcoes || [10, 20, 50, 100]}
              labelDisplayedRows={this.labelDisplayedRows}
            />
          </TableRow>
        </TableFooter>
      </Table>
    )
  }
}

export const Pagination = withStrings(PaginationComponent)
