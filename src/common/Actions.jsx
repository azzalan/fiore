import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'

export const actionsStyle = theme => ({
  actionsWrapper: {
    display: 'flex',
    padding: '20px 20px 20px 20px',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  actionIcone: {
    margin: '0 5px 0 0',
  },
})

const getButton = (actionConfig, classes) => {
  let button
  if (actionConfig.texto) {
    button = (
      <Button {...actionConfig.buttonProps}>
        {actionConfig.icone ? <actionConfig.icone className={classes.actionIcone} /> : null}
        {actionConfig.texto}
      </Button>
    )
  } else {
    button = (
      <IconButton {...actionConfig.buttonProps}>
        <actionConfig.icone />
      </IconButton>
    )
  }
  return button
}

const ActionsComponent = props => {
  const { classes, config, className } = props
  const actions = config.map((actionConfig, index) => (
    <div className={classes.action} key={index}>
      {getButton(actionConfig, classes)}
    </div>
  ))
  return <div className={className || classes.actionsWrapper}>{actions}</div>
}

ActionsComponent.propTypes = {
  className: PropTypes.string,
  config: PropTypes.array.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}

export const Actions = withStyles(actionsStyle)(ActionsComponent)
