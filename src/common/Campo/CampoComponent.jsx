import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { CampoWrapperPadrao } from './CampoWrapperPadrao'

import { validarCampo } from './validarCampo'

export class CampoComponent extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired,
    accessor: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    emptyValue: PropTypes.any,
    onValid: PropTypes.func.isRequired,
    valor: PropTypes.any,
    valorPai: PropTypes.any,
    required: PropTypes.bool,
    mensagensDeErro: PropTypes.object,
    ativarComportamentoOnFocus: PropTypes.bool,
    ativarComportamentoOnBlur: PropTypes.bool,
    onRef: PropTypes.func,
    urlValidacao: PropTypes.string,
    validar: PropTypes.func,
    onCarregando: PropTypes.func,
    onExibirErro: PropTypes.func,
    CampoWrapper: PropTypes.any,
  }

  constructor(props) {
    super(props)
    this.state = {
      isValidPadrao: false,
      isValidUrl: !props.urlValidacao,
      erros: [],
      exibirErro: false,
      helperText: null,
    }
  }

  mostrarErros = () => {
    const { _imprimirErros } = this
    const { erros } = this.state
    if (erros.length) {
      _imprimirErros(erros)
    }
  }

  onBlur = () => {
    const { _imprimirErros, _validarUrl } = this
    const { urlValidacao, valor, emptyValue } = this.props
    const { isValidPadrao, isValidUrl, erros } = this.state
    if (urlValidacao && emptyValue !== valor) {
      _validarUrl(valor)
    } else {
      if (!isValidPadrao || !isValidUrl) _imprimirErros(erros)
    }
  }

  onFocus = () => {
    const { onFocus, onExibirErro } = this.props
    const { exibirErro } = this.state
    if (exibirErro) this.setState({ exibirErro: false, helperText: '' })
    if (onFocus) onFocus()
    if (onExibirErro) onExibirErro(false)
  }

  onChange = event => {
    const { accessor, onChange, onExibirErro } = this.props
    const { exibirErro } = this.state
    if (exibirErro) this.setState({ exibirErro: false, helperText: '' })
    let novoValor = event.target.value
    onChange(novoValor, accessor)
    if (onExibirErro) onExibirErro(false)
  }

  componentDidMount = () => {
    const { _validar } = this
    const { valor, valorPai, onRef } = this.props
    if (onRef) onRef(this)
    _validar(valor, valorPai)
  }

  componentWillReceiveProps = proximoProps => {
    const { _validar } = this
    const { valor, valorPai, validarComContexto, urlValidacao } = this.props
    const novoValor = proximoProps.valor
    const novoValorPai = proximoProps.valorPai
    const temNovoValor = valor !== novoValor
    const deveValidarComContexto = valorPai !== proximoProps.valorPai && validarComContexto && !temNovoValor
    if (temNovoValor || deveValidarComContexto) {
      if (urlValidacao) this.setState({ isValidUrl: false }, () => _validar(novoValor, novoValorPai))
      else _validar(novoValor, novoValorPai)
    }
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  _validar = (valor, valorPai) => {
    let { emptyValue, mensagensDeErro, required, validar, accessor, onValid } = this.props
    const { isValidUrl } = this.state
    if (!mensagensDeErro) mensagensDeErro = {}
    const validacao = validarCampo({ emptyValue, required, valor, valorPai, mensagensDeErro, validarPorProp: validar })
    const isValid = isValidUrl && validacao.sucesso
    onValid(isValid, accessor)
    this.setState({ isValidPadrao: validacao.sucesso, erros: validacao.erros })
  }

  _validarUrl = valor => {
    // let { urlValidacao, onCarregando } = this.props
    // if (onCarregando) onCarregando(true)
    // this.setState({ carregando: true })
    // post(urlValidacao, { value: valor }).then(this._tratarRespostaDaUrlValidacao)
  }

  _tratarRespostaDaUrlValidacao = r => {
    // const { existe } = r.data
    // const { _imprimirErros } = this
    // let { mensagensDeErro, accessor, onValid, onCarregando } = this.props
    // let { erros, isValidPadrao } = this.state
    // if (!mensagensDeErro) mensagensDeErro = {}
    // const strings = store.getState().strings
    // const validacaoUrl = { sucesso: true, erros: [] }
    // if (existe) {
    //   validacaoUrl.sucesso = false
    //   validacaoUrl.erros.push({
    //     code: 'ja_existe',
    //     message: mensagensDeErro['ja_existe'] || strings.jaExiste,
    //   })
    // }
    // const isValid = isValidPadrao && validacaoUrl.sucesso
    // erros = [...validacaoUrl.erros, ...erros]
    // onValid(isValid, accessor)
    // if (onCarregando) onCarregando(false)
    // this.setState({ isValid, erros, carregando: false, isValidUrl: validacaoUrl.sucesso })
    // if (!isValid) _imprimirErros(erros)
  }

  _imprimirErros = erros => {
    const { onExibirErro } = this.props
    let helperText
    if (erros[0]) helperText = erros[0].message
    else helperText = ''
    this.setState({ exibirErro: true, helperText })
    if (onExibirErro) onExibirErro(true)
  }

  _clonarCampoEspecialiadoInjetandoProps = () => {
    let { onBlur, onFocus, onChange } = this
    const { children } = this.props
    const { exibirErro, helperText } = this.state
    const { required, ativarComportamentoOnBlur, ativarComportamentoOnFocus, valor } = this.props
    let { label } = children.props
    if (!ativarComportamentoOnBlur) onBlur = null
    if (!ativarComportamentoOnFocus) onFocus = null
    if (label && required) label += '*'
    return React.cloneElement(children, {
      error: exibirErro,
      helperText,
      label,
      onBlur,
      onFocus,
      onChange,
      value: valor,
    })
  }

  render() {
    let { CampoWrapper } = this.props
    if (!CampoWrapper) CampoWrapper = CampoWrapperPadrao
    return <CampoWrapper>{this._clonarCampoEspecialiadoInjetandoProps()}</CampoWrapper>
  }
}
