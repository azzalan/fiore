import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

const style = {
  campoWrapper: {
    margin: '0',
  },
}

const CampoWrapperPadraoComponent = props => {
  const { children, classes } = props
  return <div className={classes.campoWrapper}>{children}</div>
}

CampoWrapperPadraoComponent.propTypes = {
  children: PropTypes.element.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}

export const CampoWrapperPadrao = withStyles(style)(CampoWrapperPadraoComponent)
