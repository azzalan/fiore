import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'

const paperDefault = {
  backgroundColor: '#fafafa',
}

const paginaPaperStyle = theme => ({
  paper: {
    ...paperDefault,
    [theme.breakpoints.up('md')]: {
      margin: '10px 10px',
    },
  },
  disableTopMargin: {
    ...paperDefault,
    [theme.breakpoints.up('md')]: {
      margin: '0 0 20px 0',
    },
  },
})

const PaginaPaperComponent = props => {
  const { classes, children, disableTopMargin } = props
  return (
    <Paper className={disableTopMargin ? classes.disableTopMargin : classes.paper} elevation={1}>
      {children}
    </Paper>
  )
}

PaginaPaperComponent.propTypes = {
  children: PropTypes.any,
  disableTopMargin: PropTypes.bool,
  // style
  classes: PropTypes.object.isRequired,
}

export const PaginaPaper = withStyles(paginaPaperStyle)(PaginaPaperComponent)
