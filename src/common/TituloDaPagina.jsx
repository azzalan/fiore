import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const tituloDaPaginaStyle = theme => ({
  secao: {
    padding: '20px 20px 20px 20px',
  },
  titulo: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    [theme.breakpoints.down('xs')]: {},
  },
})

const TituloDaPaginaComponent = props => {
  const { classes, children } = props
  return (
    <div className={classes.secao}>
      <Typography className={classes.titulo} variant="title" color="primary">
        {children}
      </Typography>
    </div>
  )
}

TituloDaPaginaComponent.propTypes = {
  children: PropTypes.any,
  // style
  classes: PropTypes.object.isRequired,
}

export const TituloDaPagina = withStyles(tituloDaPaginaStyle)(TituloDaPaginaComponent)
