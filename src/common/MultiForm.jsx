import React from 'react'
import PropTypes from 'prop-types'

import { GrupoCampos } from './GrupoCampos'

export const MultiForm = props => <GrupoCampos {...props} accessor="form" />

MultiForm.propTypes = {
  onValid: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  valor: PropTypes.object.isRequired,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
}
