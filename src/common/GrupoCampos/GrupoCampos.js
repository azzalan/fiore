import React, { Component } from 'react'
import PropTypes from 'prop-types'

import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'

export class GrupoCampos extends Component {
  static propTypes = {
    accessor: PropTypes.string.isRequired,
    onValid: PropTypes.func,
    onChange: PropTypes.func,
    valor: PropTypes.object,
    valorPai: PropTypes.object,
    mensagensDeErro: PropTypes.object,
    validar: PropTypes.func,
    label: PropTypes.string,
    helperText: PropTypes.string,
    required: PropTypes.bool,
    children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
    onRef: PropTypes.func,
  }

  state = {
    camposComErro: new Set(),
    erros: [],
    carregando: false,
    mostrarErros: false,
    campos: [],
    camposRefs: {},
    isValidUrl: false,
    isValidPorProp: false,
  }

  mostrarErros = () => {
    const { camposRefs } = this.state
    for (let accessor in camposRefs) {
      const campoRef = camposRefs[accessor]
      if (campoRef.mostrarErros) campoRef.mostrarErros()
    }
  }

  componentDidMount = () => {
    const { campos, _validarGrupo, props } = this
    const { valor, valorPai, onRef } = props
    this.setState({ campos })
    _validarGrupo(valor, valorPai)
    if (onRef) onRef(this)
  }

  componentWillReceiveProps = proximoProps => {
    const { _getCampos, _validarGrupo, props } = this
    const { value, children } = props
    const newValue = proximoProps.value
    const newValuePai = proximoProps.valuePai
    const newChildren = proximoProps.children
    if (value !== newValue) {
      _validarGrupo(newValue, newValuePai)
      this.setState({ campos: _getCampos() })
    }
    if (children !== newChildren) this.setState({ campos: _getCampos() })
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  _getCampos = () => {
    const { children } = this.props
    let campos
    if (Array.isArray(children)) {
      campos = children.map(this._clonarCampoParaCampoControlado)
    } else {
      campos = [this._clonarCampoParaCampoControlado(children)]
    }
    return campos
  }

  _clonarCampoParaCampoControlado = campo => {
    const { _onCampoChange, _onCampoValid, _adicionarRefEmCamposRefs } = this
    const { valor } = this.props
    const { accessor } = campo.props
    const props = {
      valor: valor[accessor],
      valorPai: valor,
      onChange: _onCampoChange(accessor),
      onValid: _onCampoValid(accessor),
      key: accessor,
      onRef: _adicionarRefEmCamposRefs(accessor),
    }
    return React.cloneElement(campo, props)
  }

  _adicionarRefEmCamposRefs = accessor => ref => {
    const { camposRefs } = this.state
    camposRefs[accessor] = ref
    this.setState({ camposRefs })
  }

  _adicionarMensagemDeErro = erro => {
    let { mensagensDeErro } = this.props
    if (!mensagensDeErro) mensagensDeErro = {}
    if (mensagensDeErro[erro.code]) erro.message = mensagensDeErro[erro.code]
  }

  _validarPorProp = (value, valuePai) => {
    let { validar } = this.props
    let validacaoPorProp = { sucesso: true, erros: [] }
    if (validar) {
      validacaoPorProp = validar(value, valuePai)
    }
    this.setState({ validacaoPorProp })
    return validacaoPorProp
  }

  _validarGrupo = (valor, valorPai) => {
    const { onValid } = this.props
    const { camposComErro } = this.state
    const { sucesso, erros } = this._validarPorProp(valor, valorPai)
    const isValid = camposComErro.size === 0 && sucesso
    if (Array.isArray(erros)) erros.forEach(this._adicionarMensagemDeErro)
    onValid(isValid)
    this.setState({ erros })
  }

  _onCampoChange = accessor => valorCampo => {
    const { props } = this
    const { valor, onChange } = props
    valor[accessor] = valorCampo
    onChange(valor)
  }

  _onCampoValid = accessor => campoIsValid => {
    const { state, props } = this
    const { camposComErro, isValidPorProp } = state
    const { onValid } = props
    if (campoIsValid) camposComErro.delete(accessor)
    else camposComErro.add(accessor)
    const grupoIsValid = camposComErro.size === 0 && isValidPorProp
    if (onValid) onValid(grupoIsValid)
  }

  render() {
    const { erros, mostrarErros, campos } = this.state
    let { label, required, helperText } = this.props
    if (label && required) label += ' *'
    return (
      <FormControl fullWidth>
        {label && (
          <FormLabel focused style={{ margin: '0 0 10px 0' }}>
            {label}
          </FormLabel>
        )}
        {helperText && <FormHelperText>{helperText}</FormHelperText>}
        {mostrarErros && erros[0] && <FormHelperText error>{erros[0].message}</FormHelperText>}
        {campos}
      </FormControl>
    )
  }
}
