import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import { get } from 'api'

import { Pagination } from 'common/Pagination'

export class CardDisplayComponent extends Component {
  static defaultProps = {
    numeroDeColunas: {
      extraSmall: 1,
      small: 1,
      medium: 2,
      large: 3,
      extraLarge: 3,
    },
    quantidadePorPaginaInicial: 10,
    paginaInicial: 0,
    quantidadePorPaginaOpcoes: [10, 20, 50, 100],
    order: 'dataUltimaAlteracao DESC',
  }

  static propTypes = {
    quantidadeUrl: PropTypes.string.isRequired,
    fetchInstanciasUrl: PropTypes.string.isRequired,
    nadaCadastrado: PropTypes.string.isRequired,
    numeroDeColunas: PropTypes.object,
    quantidadePorPaginaInicial: PropTypes.number,
    paginaInicial: PropTypes.number,
    quantidadePorPaginaOpcoes: PropTypes.array,
    cardsInicialmenteExpandidos: PropTypes.bool,
    order: PropTypes.string,
    onRef: PropTypes.func,
    renderCard: PropTypes.func,
    // redux state
    urlState: PropTypes.object.isRequired,
    browser: PropTypes.object.isRequired,
    // style
    classes: PropTypes.object.isRequired,
    // router
    history: PropTypes.object.isRequired,
  }

  refsCards = {}

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      instancias: [],
      matrizInstancias: null,
      numeroDeColunas: null,
      numeroDeLinhas: null,
      quantidadeTotal: null,
      quantidadePorPagina: props.quantidadePorPaginaInicial,
      paginaAtual: props.paginaInicial,
      cardsInicialmenteExpandidos: props.cardsInicialmenteExpandidos,
    }
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
    this.getInstancias()
  }

  componentWillReceiveProps = proximo => {
    const { browser, urlState } = this.props
    if (browser.mediaType !== proximo.browser.mediaType) {
      this.setNumeroDeColunas(proximo)
    }
    if (urlState.filtro !== proximo.urlState.filtro) {
      this.getInstancias(proximo.urlState.filtro)
    }
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  onChangePagina = numeroDaPagina => {
    this.setState({ paginaAtual: numeroDaPagina }, () => this.getInstancias())
  }

  onChangeQuantidadePorPagina = quantidadePorPagina => {
    this.setState({ quantidadePorPagina }, this.getInstancias)
  }

  getQuantidade = filtroParam => {
    const { quantidadeUrl, urlState } = this.props
    const filtro = filtroParam ? filtroParam : urlState.filtro
    get(quantidadeUrl, filtro).then(response => {
      const data = response.data
      if (data) {
        const quantidadeTotal = data.count
        if (quantidadeTotal) {
          this.setState({ quantidadeTotal })
        }
      }
    })
  }

  getInstancias = filtroParam => {
    this.setState({ carregando: true })
    this.getQuantidade(filtroParam)
    const { fetchInstanciasUrl, order, urlState } = this.props
    const { paginaAtual, quantidadePorPagina } = this.state
    const filtro = filtroParam ? filtroParam : urlState.filtro
    const options = {
      filter: {
        order,
        limit: quantidadePorPagina,
        skip: paginaAtual * quantidadePorPagina,
        ...filtro,
      },
    }
    get(fetchInstanciasUrl, options).then(response => {
      const instancias = response.data
      this.setState({ instancias, carregando: false }, () => this.setNumeroDeColunas())
    })
  }

  setNumeroDeColunas = props => {
    let numeroDeColunas, browser
    if (props) {
      numeroDeColunas = props.numeroDeColunas
      browser = props.browser
    } else {
      numeroDeColunas = this.props.numeroDeColunas
      browser = this.props.browser
    }
    if (typeof numeroDeColunas === 'number') {
      this.setState({ numeroDeColunas }, () => this.setNumeroDeLinhas())
    } else {
      const numeroDeColunasResponsivo = numeroDeColunas[browser.mediaType]
      if (numeroDeColunasResponsivo)
        this.setState({ numeroDeColunas: numeroDeColunasResponsivo }, () => this.setNumeroDeLinhas())
      else {
        console.error(
          `props.numeroDeColunas deve conter especificação para todos os tipo de media ou ser um número. ex. 
        {
          extraSmall: 1,
          small: 1,
          medium: 2,
          large: 3,
          extraLarge: 3,
        }`,
          numeroDeColunasResponsivo,
        )
        this.setState({ numeroDeColunas: 1 }, () => this.setNumeroDeLinhas())
      }
    }
  }

  setNumeroDeLinhas = () => {
    const { numeroDeColunas, instancias } = this.state
    const maxQuantidadePorPagina = this.state.quantidadePorPagina
    const quantidadeAtual = instancias.length
    const numeroDeLinhasPadrao = maxQuantidadePorPagina / numeroDeColunas
    if (quantidadeAtual < maxQuantidadePorPagina) {
      let numeroDeLinhas = quantidadeAtual / numeroDeColunas
      const numeroDeLinhasArredondado = ~~numeroDeLinhas
      if (numeroDeLinhas > numeroDeLinhasArredondado) {
        numeroDeLinhas = numeroDeLinhasArredondado + 1
      }
      this.setState({ numeroDeLinhas }, () => this.setMatrizInstancias(instancias))
    } else {
      this.setState(
        {
          numeroDeLinhas: numeroDeLinhasPadrao,
        },
        () => this.setMatrizInstancias(instancias),
      )
    }
  }

  setMatrizInstancias = instanciasParam => {
    const { numeroDeLinhas, numeroDeColunas } = this.state
    const instancias = [...instanciasParam]
    const matrizInstancias = []
    for (let coluna = 0; coluna < numeroDeColunas; coluna++) {
      matrizInstancias.push([])
    }
    for (let linha = 0; linha < numeroDeLinhas; linha++) {
      for (let coluna = 0; coluna < numeroDeColunas; coluna++) {
        const questao = instancias.shift()
        if (questao) {
          questao.open = false
          matrizInstancias[coluna][linha] = questao
        }
      }
    }
    this.setState({ matrizInstancias })
  }

  expandirTodas = () => {
    this.setState({ cardsInicialmenteExpandidos: true })
    for (let key in this.refsCards) {
      const refCard = this.refsCards[key]
      if (refCard && refCard.expandir) refCard.expandir()
    }
  }

  contrairTodas = () => {
    this.setState({ cardsInicialmenteExpandidos: false })
    for (let key in this.refsCards) {
      const refCard = this.refsCards[key]
      if (refCard && refCard.contrair) refCard.contrair()
    }
  }

  renderColuna = (coluna, indexColuna) => {
    const { matrizInstancias } = this.state
    const { classes } = this.props
    this.refsCards[indexColuna] = []
    return (
      <div
        className={indexColuna + 1 === matrizInstancias.length ? classes.colunaOutterLast : classes.colunaOutter}
        key={indexColuna}
      >
        <div className={classes.colunaInner}>{coluna.map(this.props.renderCard(indexColuna, this))}</div>
      </div>
    )
  }

  render() {
    const { matrizInstancias, quantidadeTotal, carregando } = this.state
    const { classes, nadaCadastrado, quantidadePorPaginaInicial, quantidadePorPaginaOpcoes, selecionavel } = this.props
    let colunas = null
    if (matrizInstancias) {
      colunas = matrizInstancias.map(this.renderColuna)
    }
    const className = classes.principal + (selecionavel ? ' selecionavel' : '')
    return (
      <div className={className}>
        <div className={classes.colunas}>{colunas}</div>
        {quantidadeTotal < 1 &&
          !carregando && (
            <Paper className={classes.nadaCadastrado}>
              <Typography>{nadaCadastrado}</Typography>
            </Paper>
          )}
        {colunas &&
          quantidadeTotal && (
            <Paper className={classes.pagination}>
              <Pagination
                quantidadeTotal={quantidadeTotal}
                quantidadePorPaginaInicial={quantidadePorPaginaInicial}
                onChangePagina={this.onChangePagina}
                onChangeQuantidadePorPagina={this.onChangeQuantidadePorPagina}
                quantidadePorPaginaOpcoes={quantidadePorPaginaOpcoes}
              />
            </Paper>
          )}
      </div>
    )
  }
}
