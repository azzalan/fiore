export const cardDisplayStyle = theme => ({
  principal: {
    [theme.breakpoints.up('md')]: {
      margin: ' 0 10px',
    },
  },
  colunas: {
    display: 'flex',
    width: '100%',
    [theme.breakpoints.up('md')]: {
      margin: '10px 0 0 0',
    },
    margin: '1px 0',
  },
  colunaInner: {
    // backgroundColor: 'rgb(240, 240, 247)',
    display: 'flex',
    flexDirection: 'column',
  },
  colunaOutter: {
    width: '1%',
    flex: '1 1 auto',
    margin: '0 10px 0 0',
  },
  colunaOutterLast: {
    width: '1%',
    flex: '1 1 auto',
    margin: '0',
  },
  pagination: {
    margin: '0',
    backgroundColor: '#fafafa',
  },
  nadaCadastrado: {
    padding: '20px 20px 20px 20px',
    margin: '0 0 20px 0',
    backgroundColor: '#fafafa',
  },
})
