import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { CardDisplayComponent } from './CardDisplayComponent'
import { cardDisplayStyle } from './cardDisplayStyle'
import { cardDisplayMapStateToProps } from './cardDisplayMapStateToProps'

export const CardDisplay = compose(
  withStyles(cardDisplayStyle),
  withRouter,
  connect(cardDisplayMapStateToProps),
)(CardDisplayComponent)
