import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

export const containerStyle = theme => ({
  container: {
    margin: '0 auto',
    [theme.breakpoints.up('md')]: {
      width: 960,
    },
  },
})

const ContainerComponent = props => <div className={props.classes.container}>{props.children}</div>

ContainerComponent.propTypes = {
  children: PropTypes.any,
  // style
  classes: PropTypes.object.isRequired,
}

export const Container = withStyles(containerStyle)(ContainerComponent)
