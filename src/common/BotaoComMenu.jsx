import React, { Component } from 'react'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem'
import Popover from '@material-ui/core/Popover'
import { withStyles } from '@material-ui/core/styles'

const botaoComMenuStyle = theme => ({
  menuItem: { padding: '20px', margin: '0' },
})

class BotaoComMenuComponent extends Component {
  static propTypes = {
    children: PropTypes.any,
    menuList: PropTypes.object,
    // style
    classes: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      anchorMenu: null,
      temMenu: true,
    }
  }

  componentWillMount() {
    if (this.props.temMenu === false) {
      this.setState({
        temMenu: this.props.temMenu,
      })
    }
  }

  abrirMenu = event => {
    if (this.state.temMenu) {
      this.setState({
        anchorMenu: event.currentTarget,
      })
    }
  }

  fecharMenu = event => {
    this.setState({
      anchorMenu: null,
    })
  }

  render() {
    const { children, menuList } = this.props
    const { anchorMenu } = this.state
    return (
      <div>
        <MenuItem className={this.props.menuItem} onClick={this.abrirMenu}>
          {children}
        </MenuItem>
        <Popover
          open={Boolean(anchorMenu)}
          anchorEl={anchorMenu}
          onClick={this.fecharMenu}
          onClose={this.fecharMenu}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left',
          }}
          anchorPosition={{
            top: 64,
          }}
        >
          {menuList}
        </Popover>
      </div>
    )
  }
}

export const BotaoComMenu = withStyles(botaoComMenuStyle)(BotaoComMenuComponent)
