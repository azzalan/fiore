import React, { Component } from 'react'
import PropTypes from 'prop-types'

import TextField from '@material-ui/core/TextField'

import { Campo } from 'common/Campo'

import { ValidarUrlAdornment } from './ValidarUrlAdornment'
import { CampoTextWrapperPadrao } from './CampoTextWrapperPadrao'

export class CampoTextComponent extends Component {
  static defaultProps = {
    emptyValue: '',
  }

  static propTypes = {
    accessor: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    onValid: PropTypes.func,
    valor: PropTypes.string,
    valorPai: PropTypes.any,
    textFieldProps: PropTypes.object,
    required: PropTypes.bool,
    urlValidacao: PropTypes.string,
    mensagensDeErro: PropTypes.object,
    validar: PropTypes.func,
    CampoWrapper: PropTypes.any,
  }

  state = {
    campoRef: null,
    estaCarregando: false,
    isValid: false,
    estaExibindoErro: false,
  }

  componentDidMount = () => {
    const { onRef } = this.props
    if (onRef) onRef(this)
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  mostrarErros = () => {
    const { campoRef } = this.state
    if (campoRef.mostrarErros) campoRef.mostrarErros()
  }

  onExibirErro = estaExibindoErro => this.setState({ estaExibindoErro })

  onCarregando = estaCarregando => this.setState({ estaCarregando })

  onValid = isValid => {
    const { onValid } = this.props
    onValid(isValid)
    this.setState({ isValid })
  }

  _setCampoRef = campoRef => this.setState({ campoRef })

  render() {
    const { onValid, onCarregando, onExibirErro, _setCampoRef } = this
    let {
      accessor,
      valor,
      valorPai,
      emptyValue,
      textFieldProps,
      required,
      urlValidacao,
      mensagensDeErro,
      onChange,
      validar,
      CampoWrapper,
    } = this.props
    const { estaCarregando, isValid, estaExibindoErro } = this.state
    let inputAdornment
    if (urlValidacao) {
      inputAdornment = {
        endAdornment: (
          <ValidarUrlAdornment estaExibindoErro={estaExibindoErro} estaCarregando={estaCarregando} isValid={isValid} />
        ),
      }
    }
    if (textFieldProps === undefined) textFieldProps = {}
    return (
      <Campo
        accessor={accessor}
        onChange={onChange}
        onValid={onValid}
        valor={valor}
        valorPai={valorPai}
        required={required}
        urlValidacao={urlValidacao}
        mensagensDeErro={mensagensDeErro}
        emptyValue={emptyValue}
        ativarComportamentoOnFocus
        ativarComportamentoOnBlur
        onRef={_setCampoRef}
        validar={validar}
        onCarregando={onCarregando}
        onExibirErro={onExibirErro}
        CampoWrapper={CampoWrapper ? CampoWrapper : CampoTextWrapperPadrao}
      >
        <TextField {...textFieldProps} InputProps={inputAdornment || textFieldProps.InputProps} />
      </Campo>
    )
  }
}
