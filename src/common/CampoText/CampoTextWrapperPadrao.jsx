import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

const style = {
  campoWrapper: {
    margin: '5px 0 0 0',
  },
}

const CampoTextWrapperPadraoComponent = props => {
  const { children, classes } = props
  return <div className={classes.campoWrapper}>{children}</div>
}

CampoTextWrapperPadraoComponent.propTypes = {
  children: PropTypes.element.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}

export const CampoTextWrapperPadrao = withStyles(style)(CampoTextWrapperPadraoComponent)
