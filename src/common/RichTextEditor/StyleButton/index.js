import { withStyles } from '@material-ui/core/styles'

import { StyleButtonComponent } from './StyleButtonComponent'
import { styleButtonStyle } from './styleButtonStyle'

export const StyleButton = withStyles(styleButtonStyle)(StyleButtonComponent)
