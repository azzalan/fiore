import React, { Component } from 'react'
import PropTypes from 'prop-types'

export class StyleButtonComponent extends Component {
  static propTypes = {
    classes: PropTypes.object.isRequired,
    icon: PropTypes.any.isRequired,
    onToggle: PropTypes.func,
    style: PropTypes.string,
    active: PropTypes.bool,
  }

  onToggle = e => {
    const { onToggle, onClick } = this.props
    e.preventDefault()
    if (onToggle) onToggle(this.props.style)
    if (onClick) onClick()
  }

  render() {
    const { classes, active } = this.props
    return (
      <span className={active ? classes.activeButton : classes.buttonDefault} onMouseDown={this.onToggle}>
        {this.props.icon}
      </span>
    )
  }
}
