const COR_FONTE_FORTE = 'rgb(32, 32, 32)'
const COR_FONTE_INATIVO = 'rgba(32, 32, 32, 0.5)'

const buttonDefault = {
  color: COR_FONTE_INATIVO,
  cursor: 'pointer',
  margin: '0 8px',
  display: 'inline-block',
}

export const styleButtonStyle = {
  buttonDefault,
  activeButton: {
    ...buttonDefault,
    color: COR_FONTE_FORTE,
  },
}
