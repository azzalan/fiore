import React from 'react'
import { FormatBold, FormatItalic, FormatUnderlined } from '@material-ui/icons'

export const estilosInline = [
  { icon: <FormatBold />, style: 'BOLD' },
  { icon: <FormatItalic />, style: 'ITALIC' },
  { icon: <FormatUnderlined />, style: 'UNDERLINE' },
]
