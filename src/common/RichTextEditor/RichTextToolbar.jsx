import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

import { StyleButton } from './StyleButton'
import { estilosInline } from './estilosInline'

const toolbar = theme => ({
  boxShadow: '0px 2px 2px lightgrey',
  borderTop: `5px solid ${theme.palette.primary.main}`,
  borderRadius: '4px 4px 0px 0px',
})

const style = theme => ({
  controls: {
    display: 'flex',
    fontSize: '14px',
    userSelect: 'none',
    alignItems: 'center',
  },
  verticalRuler: {
    borderLeft: `1px solid #ddd`,
    height: '20px',
    margin: '0 8px',
  },
  toolbar: {
    ...toolbar(theme),
    position: 'absolute',
    backgroundColor: 'white',
    border: '1px solid #ddd',
    zIndex: 1,
    borderRadius: '5px',
    paddingTop: '2px',
    top: '-35px',
    right: '20px',
    borderTop: `5px solid ${theme.palette.primary.main}`,
    boxShadow: 'none',
  },
  toolbarInline: {
    ...toolbar(theme),
    width: '100%',
  },
  toolbarGroup: {
    padding: '3px 0 0 0',
  },
})

export const RichTextToolbarComponent = props => {
  const { classes, inline, currentStyle, onToggle, isHover, isActive, actions } = props
  const mostrarToolBar = inline || (!inline && (isHover || isActive))
  if (mostrarToolBar)
    return (
      <div className={inline ? classes.toolbarInline : classes.toolbar}>
        <div className={classes.controls}>
          <div className={classes.toolbarGroup}>
            {estilosInline.map((type, index) => (
              <StyleButton
                key={index}
                active={currentStyle.has(type.style)}
                icon={type.icon}
                onToggle={onToggle}
                style={type.style}
              />
            ))}
          </div>

          {actions && <div className={classes.verticalRuler} />}
          {actions && (
            <div className={classes.toolbarGroup}>
              {actions.map((action, index) => <StyleButton key={index} icon={action.icon} onClick={action.onClick} />)}
            </div>
          )}
        </div>
      </div>
    )
  else return null
}

RichTextToolbarComponent.propTypes = {
  onToggle: PropTypes.func.isRequired,
  currentStyle: PropTypes.any,
  inline: PropTypes.bool,
  isHover: PropTypes.bool,
  isActive: PropTypes.bool,
  actions: PropTypes.array,
}

export const RichTextToolbar = withStyles(style)(RichTextToolbarComponent)
