import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'

import { RichTextEditorComponent } from './RichTextEditorComponent'
import { richTextEditorStyle } from './richTextEditorStyle'

export const RichTextEditor = compose(withStyles(richTextEditorStyle))(RichTextEditorComponent)
