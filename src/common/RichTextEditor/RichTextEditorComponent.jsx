import React, { Component } from 'react'
import { Editor, EditorState, RichUtils, Modifier } from 'draft-js'
import PropTypes from 'prop-types'
import { stateToHTML } from 'draft-js-export-html'
import { stateFromHTML } from 'draft-js-import-html'

import Typography from '@material-ui/core/Typography'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'

import DoneIcon from '@material-ui/icons/Done'

import { RichTextToolbar } from './RichTextToolbar'
import { languages } from 'utils/strings'

export class RichTextEditorComponent extends Component {
  static defaultProps = {
    tabCharacter: '    ',
  }

  static propTypes = {
    onChange: PropTypes.func.isRequired,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    required: PropTypes.bool,
    children: PropTypes.any,
    actions: PropTypes.array,
    default: PropTypes.any,
    grande: PropTypes.bool,
    // config.props
    label: PropTypes.string,
    decorarCorreta: PropTypes.bool,
    inline: PropTypes.bool,
    error: PropTypes.bool,
    helperText: PropTypes.string,
    // style
    classes: PropTypes.object.isRequired,
  }

  static emptyStateContent = JSON.stringify(stateToHTML(EditorState.createEmpty().getCurrentContent()))

  constructor(props) {
    super(props)
    this.state = {
      editorState: props.default
        ? EditorState.createWithContent(stateFromHTML(props.default))
        : EditorState.createEmpty(),
      isHover: false,
      isActive: false,
    }
  }

  editor = React.createRef()

  componentDidMount() {
    this.editor.current.addEventListener('mouseenter', () => {
      this.setState({ isHover: true })
    })
    this.editor.current.addEventListener('mouseleave', () => {
      this.setState({ isHover: false })
    })
    const input = this.editor.current.querySelector('[contenteditable=true]')
    input.addEventListener('focus', () => {
      this.setState({ isActive: true })
    })
    input.addEventListener('blur', () => {
      this.setState({ isActive: false })
    })
  }

  onChange = editorState => {
    const valueFormatoDraft = editorState.getCurrentContent()
    const valueFormatoHtml = stateToHTML(valueFormatoDraft)
    this.props.onChange(valueFormatoHtml)
    this.setState({ editorState })
  }

  focusText = () => {
    this.refs.text.focus()
  }

  toggleInlineStyle = inlineStyle => {
    const { editorState } = this.state
    const newEditorState = RichUtils.toggleInlineStyle(editorState, inlineStyle)
    this.onChange(newEditorState)
  }

  hasContent = () => {
    const { editorState } = this.state
    if (editorState) {
      const { getCurrentContent } = editorState
      if (getCurrentContent) {
        const currentCotent = JSON.stringify(stateToHTML(editorState.getCurrentContent()))
        return RichTextEditorComponent.emptyStateContent !== currentCotent
      } else console.error('Tem algo de errado com o estado draft, ele está sem getCurrentContent.')
    }
    return false
  }

  onTab = e => {
    e.preventDefault()
    const { tabCharacter } = this.props
    const { editorState } = this.state
    let newContentState = Modifier.replaceText(
      editorState.getCurrentContent(),
      editorState.getSelection(),
      tabCharacter,
    )
    const newEditorState = EditorState.push(editorState, newContentState, 'insert-characters')
    this.onChange(newEditorState)
  }

  render() {
    const { editorState, isHover, isActive } = this.state
    const {
      classes,
      children,
      required,
      inline,
      label,
      actions,
      decorarCorreta,
      onFocus,
      onBlur,
      helperText,
      error,
      grande,
    } = this.props
    const hasContent = this.hasContent()
    let areaDeTextoClass
    if (grande) areaDeTextoClass = classes.areaDeTextoGrande
    else if (decorarCorreta) areaDeTextoClass = classes.areaDeTextoDecoradaCorreta
    else areaDeTextoClass = classes.areaDeTexto
    return (
      <FormControl className={classes.formControl}>
        <InputLabel
          onClick={hasContent || isActive ? null : this.focusText}
          required={required}
          shrink={hasContent || isActive}
          focused={hasContent || isActive}
          FormLabelClasses={{
            root: inline ? classes.cssLabelInline : classes.cssLabel,
            focused: classes.cssFocused,
          }}
        >
          {label}
        </InputLabel>
        <div
          onKeyPress={this.onKeyPress}
          className={decorarCorreta ? classes.rootDecoradaCorreta : classes.root}
          ref={this.editor}
        >
          {children || null}
          <RichTextToolbar
            actions={actions}
            inline={inline}
            isHover={isHover && label !== languages.ptBr.novaAlternativa}
            isActive={isActive}
            onToggle={this.toggleInlineStyle}
            currentStyle={editorState.getCurrentInlineStyle()}
          />
          <Typography className={areaDeTextoClass} headlineMapping={{ body1: 'div' }}>
            <Editor
              className={classes.editor}
              editorState={editorState}
              onChange={this.onChange}
              ref="text"
              onTab={this.onTab}
              onFocus={onFocus}
              onBlur={onBlur}
              onClick={this.focusText}
            />
            {decorarCorreta && <DoneIcon className={classes.textBoxIcon} />}
          </Typography>
        </div>
        {helperText && <FormHelperText error={error}>{helperText}</FormHelperText>}
      </FormControl>
    )
  }
}
