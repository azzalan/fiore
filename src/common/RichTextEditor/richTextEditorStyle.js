const root = theme => ({
  position: 'relative',
  margin: '15px 0 0 0',
  backgroundColor: 'white',
  border: '1px solid #ddd',
  borderRadius: '4px 4px 2px 2px',
})

const areaDeTexto = {
  display: 'flex',
  justifyContent: 'space-between',
  padding: '10px',
  cursor: 'text',
  '& .DraftEditor-root': {
    alignSelf: 'center',
    width: '100%',
    wordWrap: 'break-word',
    '& .public-DraftEditor-content': {
      wordWrap: 'break-word',
    },
  },
}

const areaDeTextoGrande = {
  minHeight: 60,
  '& .DraftEditor-root': {
    width: '100%',
    wordWrap: 'break-word',
    '& .public-DraftEditor-content': {
      minHeight: 60,
      wordWrap: 'break-word',
    },
  },
}

const cssLabel = {
  margin: '4px 0 0 10px',
  zIndex: 1,
  cursor: 'text',
  '&$cssFocused': {
    cursor: 'default',
    margin: '0 0 0 0',
  },
}

export const richTextEditorStyle = theme => ({
  formControl: {
    width: '100%',
  },
  editor: {},
  root: root(theme),
  rootDecoradaCorreta: {
    ...root(theme),
    borderColor: theme.palette.decorarCorreta,
  },
  areaDeTexto,
  areaDeTextoDecoradaCorreta: {
    ...areaDeTexto,
    '& .DraftEditor-root': {
      alignSelf: 'center',
      width: `calc(100% - 24px)`,
      wordWrap: 'break-word',
      '& .public-DraftEditor-content': {
        wordWrap: 'break-word',
      },
    },
  },
  areaDeTextoGrande: {
    ...areaDeTexto,
    ...areaDeTextoGrande,
  },
  cssLabel,
  cssLabelInline: {
    ...cssLabel,
    margin: '38px 0 0 10px',
  },
  cssFocused: {},
  textBoxIcon: {
    alignSelf: 'center',
    color: theme.palette.decorarCorreta,
  },
})
