export const styleMap = {
  CODE: {
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    padding: 2,
  },
}

export function getBlockStyle(block) {
  switch (block.getType()) {
    case 'blockquote':
      return 'RichEditor-blockquote'
    default:
      return null
  }
}

export { ESTILO_BLOCK } from './estiloBlock'
export { ESTILO_INLINE } from './estiloInline'
