import { createMuiTheme } from '@material-ui/core/styles'

export const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#54647b',
      main: '#293a4f',
      dark: '#001427',
      contrastText: '#fff',
    },
    secondary: {
      light: '#7589e4',
      main: '#415cb1',
      dark: '#003381',
      contrastText: '#fff',
    },
    decorarCorreta: 'limegreen',
  },
})
