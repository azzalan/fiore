import { dispatchToStore } from './dispatchToStore'

export const clearUrlState = () => {
  dispatchToStore('select', 'urlState', {})
}
