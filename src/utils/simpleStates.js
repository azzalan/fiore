import { auth } from 'api'

export const simpleStates = {
  usuarioAtual: auth.currentUser,
  menuEsquerdoAberto: false,
  listaDialog: [],
  urlState: {},
  loadingUser: true,
}
