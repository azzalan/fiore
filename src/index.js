import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import { MuiThemeProvider } from '@material-ui/core/styles'

import { theme } from 'utils/theme'
import { store } from 'utils/store'
import { history } from 'utils/history'

import registerServiceWorker from './registerServiceWorker'
import { App } from './App'
import 'index.css'

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <MuiThemeProvider theme={theme}>
        <App />
      </MuiThemeProvider>
    </Router>
  </Provider>,
  document.getElementById('root'),
)
registerServiceWorker()
