import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { LoungeComponent } from './LoungeComponent'
import { loungeStyle } from './loungeStyle'
import { loungeMapStateToProps } from './loungeMapStateToProps'

export const Lounge = compose(
  withStyles(loungeStyle),
  withRouter,
  connect(loungeMapStateToProps),
)(LoungeComponent)
