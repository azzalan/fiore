export const loungeStyle = theme => ({
  matchBox: {
    padding: '10px',
    width: 200,
  },
  loungeActions: {
    display: 'flex',
    justifyContent: 'center',
    margin: '10px 0',
  },
  matchOptions: {
    margin: '10px 0 0 0',
    display: 'flex',
    justifyContent: 'center',
  },
  matchesContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
})
