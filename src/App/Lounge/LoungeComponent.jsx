import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { matchCollection } from 'api'

export class LoungeComponent extends Component {
  static propTypes = {
    // redux state
    usuarioAtual: PropTypes.object.isRequired,
    // style
    classes: PropTypes.object.isRequired,
    // router
    history: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      carregando: true,
      openMatchesSnap: [],
      currentPlayer1MatchesSnap: [],
      currentPlayer2MatchesSnap: [],
    }
  }

  componentDidMount = () => {
    this.getInstancias()
  }

  getInstancias = () => {
    const { usuarioAtual } = this.props
    matchCollection
      .where('player1.uid', '==', usuarioAtual.uid)
      .where('status', '==', 'ongoing')
      .get()
      .then(this._setCurrentPlayer1Matches)
    matchCollection
      .where('player2.uid', '==', usuarioAtual.uid)
      .where('status', '==', 'ongoing')
      .get()
      .then(this._setCurrentPlayer2Matches)
    matchCollection
      .where('status', '==', 'open')
      .get()
      .then(this._setOpenMatches)
  }

  _setCurrentPlayer1Matches = currentPlayer1MatchesSnap => this.setState({ currentPlayer1MatchesSnap })

  _setCurrentPlayer2Matches = currentPlayer2MatchesSnap => this.setState({ currentPlayer2MatchesSnap })

  _setOpenMatches = openMatchesSnap => this.setState({ openMatchesSnap })

  createMatch = () => {
    matchCollection
      .add({ status: 'open' })
      .then(this._afterCreateMatch)
      .catch(e => console.log(e))
  }

  _afterCreateMatch = matchSnap => this._joinMatch(matchSnap.id)()

  _joinMatch = matchId => () => {
    const { history } = this.props
    history.push(`/match/${matchId}`)
  }

  renderMatches = openMatchesSnap => {
    const { classes } = this.props
    const render = []
    openMatchesSnap.forEach(matchRef => {
      const { _joinMatch } = this
      const matchData = matchRef.data()
      render.push(
        <Paper key={matchRef.id} className={classes.matchBox}>
          <Typography variant="caption">Id</Typography>
          <Typography>{matchRef.id}</Typography>
          <Typography variant="caption">Status</Typography>
          <Typography>{matchData.status}</Typography>
          <Typography variant="caption">Jogador esperando</Typography>
          <Typography>{matchData.player1 ? matchData.player1.email : null}</Typography>
          <div className={classes.matchOptions}>
            <Button onClick={_joinMatch(matchRef.id)}>Entrar</Button>
          </div>
        </Paper>,
      )
    })
    return render
  }

  render() {
    const { createMatch, renderMatches } = this
    const { classes } = this.props
    const { openMatchesSnap, currentPlayer1MatchesSnap, currentPlayer2MatchesSnap } = this.state
    return (
      <div>
        <div className={classes.loungeActions}>
          <Button onClick={createMatch}>Criar partida</Button>
        </div>
        <div className={classes.matchesContainer}>Em andamento</div>
        <div className={classes.matchesContainer}>
          {renderMatches(currentPlayer1MatchesSnap)}
          {renderMatches(currentPlayer2MatchesSnap)}
        </div>
        <div className={classes.matchesContainer}>Abertas</div>
        <div className={classes.matchesContainer}>{renderMatches(openMatchesSnap)}</div>
      </div>
    )
  }
}
