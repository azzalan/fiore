import { bindActionCreators } from 'redux'

export function appMapStateToProps(state) {
  return {
    usuarioAtual: state.usuarioAtual,
  }
}

export function appMapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
