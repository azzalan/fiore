import React from 'react'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'

export const BotaoMenuEsquerdo = props => (
  <IconButton color="inherit" aria-label="Menu" {...props}>
    <MenuIcon />
  </IconButton>
)
