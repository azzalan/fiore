import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'

import { MenuListPerfil } from './MenuListPerfil'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/MenuItem', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/MenuList', () => props => <div>{props.children}</div>)
jest.mock('common/Link', () => ({
  Link: props => <div>{props.children}</div>,
}))
jest.mock('local-modules/strings', () => {
  const strings = {
    minhaConta: 'minhaConta',
    sair: 'sair',
  }
  return { withStrings: Comp => props => <Comp strings={strings} /> }
})
jest.mock('utils/compositeActions', () => ({
  deslogar: jest.fn(),
}))

describe('MenuListPerfil unit tests', () => {
  test('Monta', () => {
    const component = mount(<MenuListPerfil />)
    component.unmount()
  })
  test('Strings renderizam corretamente', () => {
    const component = mount(<MenuListPerfil />)
    expect(component.contains('minhaConta')).toBeTruthy()
    expect(component.contains('sair')).toBeTruthy()
    component.unmount()
  })
})
