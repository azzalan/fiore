import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withStrings } from 'local-modules/strings'
import { BarraSuperiorComponent } from './BarraSuperiorComponent'
import { barraSuperiorMapStateToProps, barraSuperiorMapDispatchToProps } from './BarraSuperiorRedux'
import { barraSuperiorStyle } from './barraSuperiorStyle'

export const BarraSuperior = compose(
  connect(
    barraSuperiorMapStateToProps,
    barraSuperiorMapDispatchToProps,
  ),
  withStrings,
  withStyles(barraSuperiorStyle),
)(BarraSuperiorComponent)
