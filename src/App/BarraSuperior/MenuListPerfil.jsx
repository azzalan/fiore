import React from 'react'
import PropTypes from 'prop-types'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'

import { withStrings } from 'local-modules/strings'
import { Link } from 'common/Link'
import { logout } from 'api'

const MenuListPerfilComponent = props => (
  <MenuList>
    <Link to="/perfil">
      <MenuItem>{props.strings.minhaConta}</MenuItem>
    </Link>
    <MenuItem onClick={logout}>{props.strings.sair}</MenuItem>
  </MenuList>
)

MenuListPerfilComponent.propTypes = {
  strings: PropTypes.object,
}

export const MenuListPerfil = withStrings(MenuListPerfilComponent)
