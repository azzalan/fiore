import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function barraSuperiorMapStateToProps(state) {
  return {
    browser: state.browser,
    usuarioAtual: state.usuarioAtual,
  }
}

export function barraSuperiorMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      toggleMenuEsquerdoAberto: actions.toggle.menuEsquerdoAberto,
    },
    dispatch,
  )
}
