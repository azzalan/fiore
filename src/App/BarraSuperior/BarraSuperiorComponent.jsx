import React from 'react'
import PropTypes from 'prop-types'

import AppBar from '@material-ui/core/AppBar'

import { PrimeiroMenu } from './PrimeiroMenu'

export const BarraSuperiorComponent = props => {
  const { browser, toggleMenuEsquerdoAberto, usuarioAtual, classes } = props
  const renderBotaoMenuEsquerdo = browser.lessThan.medium
  const primeiroMenu = (
    <PrimeiroMenu
      renderBotaoMenuEsquerdo={renderBotaoMenuEsquerdo}
      toggleMenuEsquerdoAberto={toggleMenuEsquerdoAberto}
      usuarioAtual={usuarioAtual}
    />
  )
  if (usuarioAtual)
    return (
      <div style={{ padding: '0 !important' }}>
        {browser.greaterThan.small ? (
          <AppBar position="fixed">
            {primeiroMenu}
          </AppBar>
        ) : null}
        <div className={browser.greaterThan.small ? classes.hidden : null}>
          <AppBar position="static">
            {primeiroMenu}
          </AppBar>
        </div>
      </div>
    )
  return null
}

BarraSuperiorComponent.propTypes = {
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // redux state
  usuarioAtual: PropTypes.object,
  browser: PropTypes.object.isRequired,
  // redux actions
  toggleMenuEsquerdoAberto: PropTypes.func.isRequired,
}
