import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'

import { BotaoMenuEsquerdo } from './BotaoMenuEsquerdo'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/IconButton', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/Menu', () => props => <div>{props.children}</div>)

test('BotaoMenuEsquerdo monta', () => {
  const component = mount(<BotaoMenuEsquerdo />)
  component.unmount()
})
