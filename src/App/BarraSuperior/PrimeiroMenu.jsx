import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { withStrings } from 'local-modules/strings'

import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { BotaoComMenu } from 'common/BotaoComMenu'

import { BotaoMenuEsquerdo } from './BotaoMenuEsquerdo'
import { MenuListPerfil } from './MenuListPerfil'

const primeiroMenuStyle = theme => ({
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  colorWhite: {
    color: 'white',
  },
  menuItem: { padding: '20px 10px 20px 10px', margin: '0', color: '#293a4f' },
  letraUsuario: { fontSize: '16px' },
})

const PrimeiroComponent = props => {
  const { classes, strings, renderBotaoMenuEsquerdo, toggleMenuEsquerdoAberto, usuarioAtual } = props
  return (
    <Toolbar className={classes.flex}>
      {renderBotaoMenuEsquerdo ? (
        <div className={classes.flex}>
          <BotaoMenuEsquerdo onClick={toggleMenuEsquerdoAberto} />
        </div>
      ) : null}
      <div className={classes.flex}>
        <Typography variant="title" color="inherit">
          {strings.nomeProjeto}
        </Typography>
      </div>
      <BotaoComMenu menuList={<MenuListPerfil />}>
        <Button variant="fab" mini color="secondary" aria-label="add" className={classes.letraUsuario}>
          {usuarioAtual.email[0]}
        </Button>
      </BotaoComMenu>
    </Toolbar>
  )
}

PrimeiroComponent.propTypes = {
  usuarioAtual: PropTypes.object.isRequired,
  renderBotaoMenuEsquerdo: PropTypes.bool,
  toggleMenuEsquerdoAberto: PropTypes.func.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
}

export const PrimeiroMenu = withStrings(withStyles(primeiroMenuStyle)(PrimeiroComponent))
