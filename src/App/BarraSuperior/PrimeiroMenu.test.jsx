import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'

import { PrimeiroMenu } from './PrimeiroMenu'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/styles', () => ({
  withStyles: style => Component => Component,
}))
jest.mock('local-modules/strings', () => ({
  withStrings: Component => Component,
}))
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Typography', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/IconButton', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Button', () => props => <div>{props.children}</div>)
jest.mock('common/Icon', () => ({
  Icon: props => <div>{props.children}</div>,
}))
jest.mock('common/BotaoComMenu', () => ({
  BotaoComMenu: props => <div>{props.children}</div>,
}))
jest.mock('./BotaoMenuEsquerdo', () => ({
  BotaoMenuEsquerdo: props => <div>{props.children}</div>,
}))
jest.mock('./MenuListPerfil', () => ({
  MenuListPerfil: props => <div>{props.children}</div>,
}))

let defaultProps = {
  usuarioAtual: { data: { nome: 'nome' } },
  renderBotaoMenuEsquerdo: true,
  toggleMenuEsquerdoAberto: jest.fn(),
  // classes
  classes: {
    flex: 'flex',
    colorWhite: 'colorWhite',
    menuItem: 'menuItem',
    letraUsuario: 'letraUsuario',
  },
  // strings
  strings: {
    nomeProjeto: 'nomeProjeto',
  },
}

describe('PrimeiroMenu unit tests', () => {
  let props
  test('Monta', () => {
    props = defaultProps
    const component = mount(<PrimeiroMenu {...props} />)
    component.unmount()
  })
  test('Monta sem BotaoMenuEsquerdo', () => {
    props = { ...defaultProps, renderBotaoMenuEsquerdo: false }
    const component = mount(<PrimeiroMenu {...props} />)
    component.unmount()
  })
})
