import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'

import { withStrings } from 'local-modules/strings'
import { LoginComponent } from './LoginComponent'
import { loginStyle } from './loginStyle'
import { loginMapStateToProps, loginMapDispatchToProps } from './LoginRedux'

export const Login = compose(
  withStrings,
  withRouter,
  withStyles(loginStyle),
  connect(
    loginMapStateToProps,
    loginMapDispatchToProps,
  ),
)(LoginComponent)
