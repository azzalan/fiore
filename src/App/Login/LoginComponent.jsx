import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'

import { login } from 'api'

import { MultiForm } from 'common/MultiForm'
import { CampoText } from 'common/CampoText'

export class LoginComponent extends Component {
  static propTypes = {
    // styles
    classes: PropTypes.object.isRequired,
    // redux actions
    selectUsuarioAtual: PropTypes.func.isRequired,
    // router
    history: PropTypes.object.isRequired,
    // strings
    strings: PropTypes.object.isRequired,
  }

  constructor(props) {
    super(props)
    this.state = {
      loginFormValue: {
        email: '',
        password: '',
      },
      loginFormRef: null,
      isLoginFormValid: false,
    }
  }

  setLoginFormRef = loginFormRef => {
    this.setState({ loginFormRef })
  }

  onLoginFormValid = isLoginFormValid => {
    this.setState({ isLoginFormValid })
  }

  onLoginFormChange = loginFormValue => this.setState({ loginFormValue })

  _login = () => {
    const { loginFormValue } = this.state
    const { email, password } = loginFormValue
    login(email, password).catch(e => console.log(e))
  }

  _mostrarErros = () => {
    if (this.state.loginFormRef) this.state.loginFormRef.mostrarErros()
  }

  render() {
    const { setLoginFormRef, onLoginFormValid, onLoginFormChange, _login } = this
    const { loginFormValue } = this.state
    const { classes, strings } = this.props
    return (
      <div className={classes.background}>
        <div className={classes.loginContainerOut}>
          <div>
            <div className={classes.formBox} dir="x">
              <Paper className={classes.paperPadding} elevation={2}>
                <div className={classes.helperText} />
                <MultiForm
                  onValid={onLoginFormValid}
                  onChange={onLoginFormChange}
                  valor={loginFormValue}
                  onRef={setLoginFormRef}
                >
                  <CampoText
                    accessor="email"
                    required
                    textFieldProps={{
                      label: strings.email,
                      fullWidth: true,
                    }}
                  />
                  <CampoText
                    accessor="password"
                    required
                    textFieldProps={{
                      label: strings.senha,
                      fullWidth: true,
                      type: 'password',
                    }}
                  />
                </MultiForm>
                <div className={classes.buttonContainer}>
                  <Button className={classes.button} color="primary" onClick={_login}>
                    {strings.entrar}
                  </Button>
                </div>
              </Paper>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
