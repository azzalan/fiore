import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function loginMapStateToProps(state) {
  return {}
}

export function loginMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectUsuarioAtual: actions.select.usuarioAtual,
    },
    dispatch,
  )
}
