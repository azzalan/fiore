export const loginStyle = theme => ({
  background: {
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'cover',
    minHeight: '100vh',
    display: 'flex',
    overflow: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto, sans-serif',
  },
  iconButton: {
    marginLeft: '24px',
  },
  loginContainerOut: {
    minWidth: '100%',
    minHeight: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto, sans-serif',
  },
  formBox: {
    [theme.breakpoints.down('sm')]: {
      margin: '0px',
    },
    [theme.breakpoints.up('sm')]: {
      margin: '0px 2px 4px 2px',
    },
    display: 'flex',
    justifyContent: 'center',
  },
  paperPadding: {
    padding: '5px 20px 5px 20px',
    borderRadius: '5px',
    width: 280,
  },
  tituloPaper: {
    margin: '0px 2px 4px 2px',
    textAlign: 'center',
  },
  buttonContainer: {
    padding: '0',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    margin: '0 0 15px 0',
  },
  emailAdornment: { marginBottom: '4px', marginRight: '12px', color: 'rgba(0, 0, 0,0.5)' },
  helperText: { justifyContent: 'center', display: 'flex', margin: '0 0 5px 0' },
})
