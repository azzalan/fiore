import { compose } from 'redux'
import { withStyles } from '@material-ui/core/styles'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { MatchComponent } from './MatchComponent'
import { matchStyle } from './matchStyle'
import { matchMapStateToProps } from './matchMapStateToProps'

export const Match = compose(
  withStyles(matchStyle),
  withRouter,
  connect(matchMapStateToProps),
)(MatchComponent)
