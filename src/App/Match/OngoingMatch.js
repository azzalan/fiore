import React from 'react'
import Proptypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import GradeIcon from '@material-ui/icons/Grade'

const style = {
  ongoingMatchContainer: {
    margin: '10px',
  },
  gameActions: {
    display: 'flex',
    justifyContent: 'center',
  },
  roundContainer: {
    margin: '10px',
    padding: '10px',
  },
}

const getOponentName = (currentPlayer, player1, player2) => {
  switch (currentPlayer) {
    case 'player1':
      return player1.email
    case 'player2':
      return player2.email
    default:
      return 'Jogador misterioso'
  }
}

const renderEnergy = quant => {
  const render = []
  for (let i = 0; i < quant; i++) {
    render.push(<GradeIcon key={i} color="primary" />)
  }
  return render
}

const OngoingMatchComponent = props => {
  const { player1, player2, currentPlayer, classes } = props
  return (
    <div className={classes.ongoingMatchContainer}>
      <Typography variant="title" align="center">
        Derrote {getOponentName(currentPlayer, player1, player2)} em duelo
      </Typography>

      <Paper className={classes.roundContainer}>
        <Typography variant="caption">Força</Typography>
        {renderEnergy(4)}
        <Typography variant="caption">Energia</Typography>
        {renderEnergy(10)}
      </Paper>
      <Paper className={classes.roundContainer}>
        <Typography variant="title" align="center">
          Round 1
        </Typography>
        <Typography variant="caption" align="center">
          Ação do oponente
        </Typography>
        <Typography align="center">Ataque</Typography>
        <Typography variant="caption" align="center">
          Resolução
        </Typography>
        <Typography align="center">Você defendeu o inimigo com successo</Typography>
        <Typography variant="caption" align="center">
          Minha ação
        </Typography>
        <Typography align="center">Defesa</Typography>
      </Paper>

      <Paper className={classes.roundContainer}>
        <Typography variant="caption">Força</Typography>
        {renderEnergy(8)}
        <Typography variant="caption">Energia</Typography>
        {renderEnergy(10)}
      </Paper>
      <div className={classes.gameActions}>
        <Button>Atacar</Button>
        <Button>Defender</Button>
        <Button>Finta</Button>
        <Button>Não fazer nada</Button>
      </div>
    </div>
  )
}

OngoingMatchComponent.propTypes = {
  player1: Proptypes.object.isRequired,
  player2: Proptypes.object.isRequired,
  currentPlayer: Proptypes.string.isRequired,
}

export const OngoingMatch = withStyles(style)(OngoingMatchComponent)
