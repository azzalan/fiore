import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { OngoingMatch } from './OngoingMatch'

import { match } from 'api'

export class MatchComponent extends Component {
  static propTypes = {
    // style
    classes: PropTypes.object.isRequired,
    // router
    history: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    // redux state
    usuarioAtual: PropTypes.object,
  }

  constructor(props) {
    super(props)
    this.state = {
      unsubscribeMatch: null,
      unsubscribeTurnCollection: null,
      turnCollection: null,
      player1: null,
      player2: null,
      currentTurn: 0,
      matchStatus: null,
      currentPlayer: null,
    }
  }

  componentDidMount = () => {
    const { props, _getMatch } = this
    const { id } = props.match.params
    _getMatch(id)
  }

  componentWillUnmount = () => {
    const { unsubscribeMatch, unsubscribeTurnCollection } = this.state
    if (unsubscribeMatch) unsubscribeMatch()
    if (unsubscribeTurnCollection) unsubscribeTurnCollection()
  }

  _getMatch = id => this._listenMatch(match(id))

  _listenMatch = match => {
    const unsubscribeMatch = match.onSnapshot(this._setMatch)
    const turnCollection = match.turns
    const unsubscribeTurnCollection = turnCollection.onSnapshot(this._setTurns)
    this.setState({ unsubscribeMatch, unsubscribeTurnCollection, turnCollection })
  }

  _setTurns = turnCollectionSnap => {
    console.log(turnCollectionSnap)
  }

  _setMatch = matchSnap => {
    let { player1, player2, status } = matchSnap.data()
    this.setState({ player1, player2, matchStatus: status }, this._checkCurrentPlayer(matchSnap))
  }

  _checkCurrentPlayer = matchSnap => () => {
    let { currentPlayer } = this.state
    if (!currentPlayer) this._setCurrentPlayer(matchSnap)
  }

  _setCurrentPlayer = matchSnap => {
    const { player1, player2 } = matchSnap.data()
    const { usuarioAtual } = this.props
    const { email, displayName, uid } = usuarioAtual
    const usuarioAtualData = { email, displayName, uid }
    let currentPlayer
    if (player1 && player1.uid === usuarioAtual.uid) currentPlayer = 'player1'
    else if (player2 && player2.uid === usuarioAtual.uid) currentPlayer = 'player2'
    else if (!player1) {
      currentPlayer = 'player1'
      matchSnap.ref.update({ player1: usuarioAtualData })
    } else if (!player2 && player1.uid !== uid) {
      currentPlayer = 'player2'
      matchSnap.ref.update({ player2: usuarioAtualData, status: 'ongoing' })
    }
    this.setState({ currentPlayer })
  }

  render() {
    let { player1, player2, currentPlayer, matchStatus } = this.state
    return (
      <div>
        {matchStatus === 'ongoing' && (
          <OngoingMatch player1={player1} player2={player2} currentPlayer={currentPlayer} />
        )}
        {matchStatus === 'open' && <div onClick={() => this.state.turnCollection.add({})}>Esperando outro jogador</div>}
      </div>
    )
  }
}
