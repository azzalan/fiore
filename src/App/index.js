import { AppComponent } from './AppComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { withStyles } from '@material-ui/core/styles'

import { appMapStateToProps, appMapDispatchToProps } from './AppRedux'
import { appStyle } from './appStyle'

export const App = compose(
  withStyles(appStyle),
  withRouter,
  connect(
    appMapStateToProps,
    appMapDispatchToProps,
  ),
)(AppComponent)
