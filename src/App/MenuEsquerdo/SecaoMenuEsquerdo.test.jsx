import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'

import { SecaoMenuEsquerdo } from './SecaoMenuEsquerdo'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/Divider', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Typography', () => props => <div>{props.children}</div>)

const defaultProps = {
  classes: {},
  children: <div>Conteúdo</div>,
  nome: 'nome',
}

describe('SecaoMenuEsquerdo unit tests', () => {
  test('Monta', () => {
    const component = mount(<SecaoMenuEsquerdo {...defaultProps} />)
    component.unmount()
  })
  test('Rederiza children', () => {
    const component = mount(<SecaoMenuEsquerdo {...defaultProps} />)
    expect(component.contains(defaultProps.children)).toBe(true)
    component.unmount()
  })
  test('Rederiza nome', () => {
    const component = mount(<SecaoMenuEsquerdo {...defaultProps} />)
    expect(component.contains(defaultProps.nome)).toBe(true)
    component.unmount()
  })
})
