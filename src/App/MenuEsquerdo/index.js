import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { withStrings } from 'local-modules/strings'
import { MenuEsquerdoComponent } from './MenuEsquerdoComponent'
import { menuEsquerdoStyle } from './menuEsquerdoStyle'
import { menuEsquerdoMapStateToProps, menuEsquerdoMapDispatchToProps } from './MenuEsquerdoRedux'

export const MenuEsquerdo = compose(
  withStrings,
  withStyles(menuEsquerdoStyle),
  connect(
    menuEsquerdoMapStateToProps,
    menuEsquerdoMapDispatchToProps,
  ),
)(MenuEsquerdoComponent)
