import React from 'react'
import PropTypes from 'prop-types'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'

export const SecaoMenuEsquerdo = props => (
  <div>
    <Divider />
    <Typography className={props.classes.tituloSecao} align="center" color="primary" variant="button">
      {props.nome}
    </Typography>
    <Divider />
    {props.children}
  </div>
)

SecaoMenuEsquerdo.propTypes = {
  classes: PropTypes.object,
  children: PropTypes.any,
  nome: PropTypes.string,
}
