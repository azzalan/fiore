import React from 'react'
import PropTypes from 'prop-types'

import SwipeableDrawer from '@material-ui/core/SwipeableDrawer'
import MenuItem from '@material-ui/core/MenuItem'

import Home from '@material-ui/icons/Home'

import { Link } from 'common/Link'

export class MenuEsquerdoComponent extends React.Component {
  fecharMenuEsquerdo = () => this.props.selectMenuEsquerdoAberto(false)

  abrirMenuEsquerdo = () => this.props.selectMenuEsquerdoAberto(true)

  render() {
    const { classes, strings, menuEsquerdoAberto, usuarioAtual } = this.props
    if (usuarioAtual)
      return (
        <SwipeableDrawer open={menuEsquerdoAberto} onClose={this.fecharMenuEsquerdo} onOpen={this.abrirMenuEsquerdo}>
          <div tabIndex={0} role="button" onClick={this.fecharMenuEsquerdo} onKeyDown={this.fecharMenuEsquerdo}>
            <Link to="/">
              <MenuItem className={classes.menuItem}>
                <Home className={classes.rightIcon} />
                {strings.dashboard}
              </MenuItem>
            </Link>
          </div>
        </SwipeableDrawer>
      )
    return null
  }
}

MenuEsquerdoComponent.propTypes = {
  // redux state
  strings: PropTypes.object.isRequired,
  menuEsquerdoAberto: PropTypes.bool,
  usuarioAtual: PropTypes.object,
  // redux actions
  selectMenuEsquerdoAberto: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
}
