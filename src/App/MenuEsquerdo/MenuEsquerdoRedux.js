import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function menuEsquerdoMapStateToProps(state) {
  return {
    menuEsquerdoAberto: state.menuEsquerdoAberto,
    usuarioAtual: state.usuarioAtual,
  }
}

export function menuEsquerdoMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectMenuEsquerdoAberto: actions.select.menuEsquerdoAberto,
    },
    dispatch,
  )
}
