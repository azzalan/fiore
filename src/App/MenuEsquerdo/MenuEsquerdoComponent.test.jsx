import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import React from 'react'

import { MenuEsquerdoComponent } from './MenuEsquerdoComponent'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('@material-ui/core/SwipeableDrawer', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/Home', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/ViewList', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/icons/AssignmentInd', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/MenuItem', () => props => <div>{props.children}</div>)
jest.mock('@material-ui/core/Toolbar', () => props => <div>{props.children}</div>)

jest.mock('common/Link', () => ({
  Link: props => <div>{props.children}</div>,
}))

const defaultProps = {
  // redux state
  usuarioAtual: {
    isDocente: jest.fn(),
    isAdmin: jest.fn(),
  },
  strings: {},
  menuEsquerdoAberto: false,
  // redux actions
  selectMenuEsquerdoAberto: jest.fn(),
  // style
  classes: {},
}

const mixinPropsMenuEsquerdoAbertoTrue = {
  menuEsquerdoAberto: true,
}

const mixinPropsMenuEsquerdoAbertoFalse = {
  menuEsquerdoAberto: false,
}

describe('MenuEsquerdoComponent unit tests', () => {
  test('Monta', () => {
    const component = mount(<MenuEsquerdoComponent {...defaultProps} />)
    component.unmount()
  })
  test('Com menuEsquerdoAberto true, SwipeableDrawer props.open é true', () => {
    const props = { ...defaultProps, ...mixinPropsMenuEsquerdoAbertoTrue }
    const component = mount(<MenuEsquerdoComponent {...props} />)
    component.unmount()
  })
  test('Com menuEsquerdoAberto false, SwipeableDrawer pros.open é false', () => {
    const props = { ...defaultProps, ...mixinPropsMenuEsquerdoAbertoFalse }
    const component = mount(<MenuEsquerdoComponent {...props} />)
    component.unmount()
  })
  test('Métodos de fechar e abrir menu implementados', () => {
    const component = mount(<MenuEsquerdoComponent {...defaultProps} />)
    component.instance().fecharMenuEsquerdo()
    component.instance().abrirMenuEsquerdo()
    expect(defaultProps.selectMenuEsquerdoAberto).toBeCalledTimes(2)
    component.unmount()
  })
})
