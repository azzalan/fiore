import React from 'react'
import PropTypes from 'prop-types'

import { withStrings } from 'local-modules/strings'

import { TituloDaPagina } from 'common/TituloDaPagina'
import { PaginaPaper } from 'common/PaginaPaper'

const PaginaNaoEncontradaComponent = props => (
  <PaginaPaper>
    <TituloDaPagina>{props.strings.paginaNaoEncontrada}</TituloDaPagina>
  </PaginaPaper>
)

PaginaNaoEncontradaComponent.propTypes = {
  // strings
  strings: PropTypes.object.isRequired,
}

export const PaginaNaoEncontrada = withStrings(PaginaNaoEncontradaComponent)
