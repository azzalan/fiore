import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'

import { Dialog } from 'common/Dialog'

import { redirecionamentoDeRotas } from './redirecionamentoDeRotas'
import { Display } from './Display'
import { Login } from './Login'
import { MenuEsquerdo } from './MenuEsquerdo'
import { PaginaNaoEncontrada } from './PaginaNaoEncontrada'
import { Lounge } from './Lounge'
import { Match } from './Match'
import { listenUserChange } from 'api/onAuthStateChanged'

export class AppComponent extends Component {
  static propTypes = {
    // redux state
    usuarioAtual: PropTypes.object,
    loadingUser: PropTypes.bool,
    // style
    classes: PropTypes.object.isRequired,
    // router
    history: PropTypes.object.isRequired,
  }

  state = {
    unsubscribeUserChange: listenUserChange(),
  }

  componentWillReceiveProps = novosProps => {
    const { loadingUser, usuarioAtual } = this.props
    const userChanged = usuarioAtual !== novosProps.usuarioAtual
    const loadingUserChanged = loadingUser !== novosProps.loadingUser
    if (!novosProps.loadingUser && (userChanged || loadingUserChanged))
      redirecionamentoDeRotas(novosProps.usuarioAtual, novosProps.history)
  }

  componentWillUnmount = () => this.state.unsubscribeUserChange()

  render() {
    const { classes, usuarioAtual, loadingUser } = this.props
    if (!loadingUser)
      return (
        <div className={classes.app}>
          <MenuEsquerdo />
          <Switch>
            <Route exact path="/login" component={Login} />
            {usuarioAtual && (
              <Display>
                <div>
                  <Switch>
                    <Route exact path="/match/:id" component={Match} />
                    <Route exact path="/" component={Lounge} />
                    <Route path="/" component={PaginaNaoEncontrada} />
                  </Switch>
                </div>
              </Display>
            )}
          </Switch>
          <Dialog />
        </div>
      )
  }
}
