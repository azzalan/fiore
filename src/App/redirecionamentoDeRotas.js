export const redirecionamentoDeRotas = (usuarioAtual, history) => {
  const { pathname } = history.location
  if (usuarioAtual && pathname === '/login') history.push('/')
  else if (!usuarioAtual && pathname !== '/login') history.push('/login')
}
