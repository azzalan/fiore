import { operations } from './operations'
import { withData, withoutData } from './actionBuilder'

export const createSimpleActions = stateKeys => {
  const actions = {}
  for (let operationKey in operations) {
    actions[operationKey] = {}
    for (let stateKey in stateKeys) {
      actions[operationKey][stateKey] = operations[operationKey].usesData
        ? withData(operationKey, stateKey)
        : withoutData(operationKey, stateKey)
    }
  }
  return actions
}
