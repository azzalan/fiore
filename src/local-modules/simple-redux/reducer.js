import { getNewState } from './getNewState'

export const reducer = (stateKey, initial = null) => {
  return (currentState = initial, action) => {
    return getNewState(currentState, action, stateKey)
    /* switch (action.type) {
      case 'select' + stateKey:
        return action.payload
      case 'TOGGLE' + stateKey:
        if (typeof state !== 'boolean') throw new TypeError('A action toggle só é válida para valores booleanos.')
        return !currentState
      default:
        break
    }
    return currentState */
  }
}
