const UID = 'f1ca6670-e55b-49f2-b9db-8b9c57cea78a'

export const createStringsReducer = (languages, uid = UID) => (state, action) => {
  if (!state) return languages.ptBr
  switch (action.type) {
    case 'SELECT' + uid:
      return action.payload
    default:
      break
  }
  return state
}
